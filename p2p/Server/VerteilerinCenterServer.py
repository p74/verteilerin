import socket
import glob
import threading
import os

svrCount = 2

def dispatchThread(conn, addr):
    print('Responding...\n')

    from_clnt = ''

    while True:
        data = conn.recv(4069).decode('utf-8')

        if not data:
            break

        from_clnt += data
        print("Got: ", from_clnt)

        if ('wfile:' in from_clnt):
            fileName = from_clnt[6:from_clnt.index('|')]
            print(fileName)
            svrList = list()
            for i in range (2, svrCount+2, 1):
                subsvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                subsvr.connect(('127.0.0.' + str(i), 9090))
                subsvr.send('cmd:list'.encode())
                from_subsvr = subsvr.recv(4096)
                subsvr.close()
                if fileName in (from_subsvr.decode()):
                    svrList.append('127.0.0.' + str(i))

            if len(svrList) != 0:
                for svrIP in svrList:
                    subsvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    subsvr.connect((svrIP, 9090))
                    subsvr.send(from_clnt.encode())
                    subsvr.close()
            else:
                print('404')

        else:
            subsvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            subsvr.connect(('127.0.0.' + '2', 9090))
            subsvr.send(from_clnt.encode())
            conn.send(subsvr.recv(4096))



svrIP = '127.0.0.1'
svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svr.bind((svrIP, 9090))
svr.listen(5)
print("\n\tThe main server is online...\n")

while True:
    print('Waiting...')
    conn, addr = svr.accept()
    dispatchThread(conn, addr)
    #threading.Thread(target = dispatchThread, args = [conn, addr]).start()
