import socket
import glob
import threading
import os

dataBaseAddress = '.\\database1\\'

def dispatchThread(conn, addr):
    print('Thread Created.\n')

    from_clnt = ''

    while True:
        data = conn.recv(4069).decode('utf-8')

        if not data:
            break

        from_clnt += data
        print("Got: ", from_clnt)

        if (from_clnt == 'cmd:list'):
            conn.send(sendList())

        elif ('wfile:' in from_clnt):
            fileName = from_clnt[6:from_clnt.index('|')]
            fileData = from_clnt[from_clnt.index('|'):]
            writeFile(fileName, fileData)

        elif ('file:' in from_clnt):
            fileName = from_clnt[5:]
            fileData = sendFile(fileName)
            if (type(fileData).__name__ != 'bool'):
                conn.sendall(fileData)
            else:
                conn.send(askFromAnotherServrer(fileName))
                #conn.send(sendError404(fileName))

        else:
            conn.send(sendError404(from_clnt))

    conn.close()
    print('client disconnected')

def writeFile(fileName, fileData):
    with open(os.path.join(dataBaseAddress, fileName), 'w') as file2write:
        file2write.write(fileData)
        file2write.close()

def askFromAnotherServrer(fileName):
    nextSvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    nextSvr.connect((nextIP(), 9090))
    nextSvr.send(('file:' + fileName).encode())
    from_nextSvr = ''

    with open(os.path.join(dataBaseAddress, fileName), 'a+') as file2write:
        while (True):
            from_nextSvr = nextSvr.recv(4096)

            if (not from_nextSvr):
                break

            file2write.write(from_nextSvr.decode())
            break

        file2write.close()
     
    nextSvr.close()

    return(sendFile(fileName))


def sendList():
    print(dataBaseAddress)
    docsList = glob.glob(dataBaseAddress + '*.*')
    print(docsList)
    docsStr = '|'.join(map(str, docsList))
    print("GOT HERE\t", docsStr)
    return docsStr.encode("utf-8")

def sendFile(fileName):
    fileAddress = dataBaseAddress + fileName
    #fileData = bytes('', 'utf-8')
    fileData = ''
    print(fileAddress)
    
    if (os.path.exists(fileAddress)):
        #with open(fileAddress, 'rb', encoding="utf-8") as fileToSend:
        with open(fileAddress, 'r', encoding="utf-8") as fileToSend:
            for data in fileToSend:
                fileData += data
        #return fileData
        return fileData.encode('utf-8')

    else:
        print('AN ERROR!')
        return False


def sendError404(msg):
    return ("\'{}\' is not recognized as a command or a file."
        .format(msg)
            .encode('utf-8'))

def nextIP():
    return '127.0.0.' + str(int(svrIP[-1]) + 1)

svrIP = input('Bind an IP to connect: ')
dataBaseAddress = input('Enter a database path: ')#.replace('\\', '\\\\')
print(dataBaseAddress)
svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svr.bind((svrIP, 9090))
svr.listen(5)
print("\n\tThe server is online...\n")

while True:
    print('Waiting...')
    conn, addr = svr.accept()
    print('Creating Thread...')
    threading.Thread(target = dispatchThread, args = [conn, addr]).start()