import socket
import glob
import threading
import os

dataBaseAddress = '.\\data\\'

def dispatchThread(conn, addr):
    print('Thread Created.\n')

    from_clnt = ''

    while True:
        data = conn.recv(4069).decode('utf-8')

        if not data:
            break

        from_clnt += data
        print("Got: ", from_clnt)

        if (from_clnt == 'cmd:list'):
            conn.send(sendList())

        elif ('file:' in from_clnt):
            fileName = from_clnt[5:]
            fileData = sendFile(fileName)
            if (type(fileData).__name__ != 'bool'):
                conn.sendall(fileData)
            else:
                conn.send(sendError404(fileName))

        else:
            conn.send(sendError404(from_clnt))

    conn.close()
    print('client disconnected')

def sendList():
    print(dataBaseAddress)
    docsList = glob.glob(dataBaseAddress + '*.*')
    docsStr = '|'.join(map(str, docsList))
    return docsStr.encode("utf-8")

def sendFile(fileName):
    fileAddress = dataBaseAddress + fileName
    #fileData = bytes('', 'utf-8')
    fileData = ''
    
    if (os.path.exists(fileAddress)):
    #with open(fileAddress, 'rb', encoding="utf-8") as fileToSend:
        with open(fileAddress, 'r', encoding="utf-8") as fileToSend:
            for data in fileToSend:
                fileData += data
        #return fileData
        return fileData.encode('utf-8')

    else:
        return False


def sendError404(msg):
    return ("\'{}\' is not recognized as a command or a file."
        .format(msg)
            .encode('utf-8'))

svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svr.bind(('127.0.0.1', 9090))
svr.listen(5)
print("\n\tThe server is online...\n")

while True:
    print('Waiting...')
    conn, addr = svr.accept()
    print('Creating Thread...')
    threading.Thread(target = dispatchThread, args = [conn, addr]).start()