import socket
import os
import threading

def parser(cmd):
    clnt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clnt.connect(('127.0.0.1', 9090))
    cmd = cmd.lstrip().strip()
    print('Running:', '\'{}\''.format(cmd))

    if ('wfile:' in cmd):
        fileData = ''
        with open(os.path.join('.', cmd[6:]), 'r', encoding="utf-8") as file2read:
            for data in file2read:
                fileData += data
        file2read.close()

        cmd += '|' + fileData
        clnt.send(cmd.encode())

    elif ('file:' in cmd):
        clnt.send(cmd.encode())

        with open(os.path.join('.', cmd[5:]), 'a+') as file2write:
            while (True):
                from_svr = clnt.recv(4096)

                if (not from_svr):
                    break

                #print(from_svr)
                file2write.write(from_svr.decode())
                break

            file2write.close()
     
        clnt.close()

        """
        clnt.send('cmd:list'.encode())
        list_from_svr = clnt.recv(4096)
        fileList = list_from_svr.decode().split('|')
        print("The List", fileList)

        if (('.\\data\\' + cmd[5:]) in fileList):
            clnt.sendall(cmd.encode())
            with open(os.path.join('.', cmd[5:]), 'a+') as file2write:
                while (True):
                    from_svr = clnt.recv(4096)

                    if (not from_svr):
                        break

                    #print(from_svr)
                    file2write.write(from_svr.decode())
                    break
                file2write.close()

        else:
            print("NO!")
     
        clnt.close()
        """

    else:
        clnt.send(cmd.encode())
        from_svr = clnt.recv(4096)
        clnt.close()
        print(from_svr.decode())

while (True):
    exitFlag = False
    inputcmd = input('~> ')
    cmdList = inputcmd.split('&')
    for cmd in cmdList:
        if (cmd.lstrip().strip() == 'exit'):
            exitFlag = True
        else:
            threading.Thread(target = parser, args = [cmd]).start()
    if exitFlag:
        break