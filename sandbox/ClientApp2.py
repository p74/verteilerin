import socket
import os

while (True):
    clnt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clnt.connect(('127.0.0.1', 9090))
    cmd = input('~> ')
    
    if (cmd == 'exit'):
        clnt.close()
        break

    elif ('file:' in cmd):
        clnt.send(cmd.encode())

        with open(os.path.join('.', cmd[5:]), 'a+') as file2write:
            while (True):
                from_svr = clnt.recv(4096)

                if (not from_svr):
                    break

                #print(from_svr)
                file2write.write(from_svr.decode())
                break

            file2write.close()
     
        clnt.close()

    else:
        clnt.send(cmd.encode())
        from_svr = clnt.recv(4096)
        clnt.close()
        print(from_svr.decode())

#clnt.send('cmd:list'.encode())
#clnt.send('file:Text0004.txt'.encode())