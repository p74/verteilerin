import socket
import glob
import threading
#import queue

def dispatcher(conn, addr):
    print('Thread Created.\n')
    docsList = glob.glob(".\\data\\*")
    docsStr = '|'.join(map(str, docsList))
    from_clnt = ''

    while True:
        data = conn.recv(4069).decode('utf-8')

        if not data:
            break

        from_clnt += data
        print("Got: ", from_clnt)

        if (from_clnt == 'cmd:list'):
            conn.send(docsStr.encode())

        elif ('file:' in from_clnt):
            fileName = from_clnt[5:]
            fileAdd = '.\\data\\' + fileName
            with open(fileAdd, 'rb') as file2send:
                for datum in file2send:
                    conn.sendall(datum)

        else:
            conn.send("\'{}\' is not recognized as a command or a file."
                .format(from_clnt)
                    .encode())

    conn.close()
    print('client disconnected')

svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svr.bind(('127.0.0.1', 9090))
svr.listen(5)
print("\n\tThe server is online...\n")

while True:
    print('Waiting...')
    conn, addr = svr.accept()
    print('Creating Thread...')
    threading.Thread(target = dispatcher, args = [conn, addr]).start()