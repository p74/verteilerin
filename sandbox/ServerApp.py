import socket
import glob

svr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svr.bind(('127.0.0.1', 9090))
svr.listen(5)
print("\n\tThe server is online...\n")

docsList = glob.glob(".\\data\\*.txt")
docsStr = '|'.join(map(str, docsList))

while True:
    print("waiting...")
    conn, addr = svr.accept()
    print(type(conn))
    from_clnt = ''

    while True:
        data = conn.recv(4069).decode('utf-8')
        if not data:
            break
        from_clnt += data
        print(from_clnt)

        print(from_clnt == ('cmd:list'))
        if from_clnt == ('cmd:list'()):
            conn.send(docsStr.encode())

        else:
            conn.send('I am a server!'.encode())

    conn.close()
    print('client disconnected')